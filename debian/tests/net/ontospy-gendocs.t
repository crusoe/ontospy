use strict;
use warnings;

use Test::More;
use File::HomeDir::Test;
use File::HomeDir;
use File::Path qw(make_path);
use Test::TempDir::Tiny;
use Test::Command::Simple;

# network access may be absent or unreliable
plan skip_all => 'network test skipped when EXTENDED_TESTING is unset'
	unless ($ENV{EXTENDED_TESTING});

my @CMD = qw/ontospy gendocs/;
my $CMD = join ' ', @CMD;

ok $ENV{HOME} = File::HomeDir->my_home, 'create fake $HOME';
ok make_path "$ENV{HOME}/.ontospy/models", 'create $HOME/.ontospy/models';

in_tempdir "gendocs network" => sub {
	run_ok @CMD, qw(--type 1 http://xmlns.com/foaf/spec/);
	cmp_ok stdout, 'eq', '', 'Testing stdout';
	like stderr, qr/Loading graph.*\nBuilding visualization/, 'Testing stderr';
};

done_testing;
