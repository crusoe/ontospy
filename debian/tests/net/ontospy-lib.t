use strict;
use warnings;

use Test::More;
use File::HomeDir::Test;
use File::HomeDir;
use File::Path qw(make_path);
use Test::TempDir::Tiny;
use Test::Command::Simple;

# network access may be absent or unreliable
plan skip_all => 'network test skipped when EXTENDED_TESTING is unset'
	unless ($ENV{EXTENDED_TESTING});

my @CMD = qw/ontospy lib/;
my $CMD = join ' ', @CMD;

ok $ENV{HOME} = File::HomeDir->my_home, 'create fake $HOME';
ok make_path "$ENV{HOME}/.ontospy/models", 'create $HOME/.ontospy/models';

in_tempdir "lib bootstrap" => sub {
	run_ok 'sh', '-c', "yes | $CMD --bootstrap";
	like stdout, qr/2\n3\n4\n5\n6\n7\n8/, 'Testing stdout';
	like stderr, qr/Bootstrap command completed/, 'Testing stderr';
};

done_testing;
