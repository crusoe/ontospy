use strict;
use warnings;

use Test::More;
use Test::Command::Simple;

my @CMD = qw/ontospy scan/;
my $CMD = join ' ', @CMD;

run_ok @CMD;
cmp_ok stdout, 'eq', '', 'Testing stdout';
like stderr, qr/Usage: $CMD/, 'Testing stderr';

run_ok @CMD, '--help';
like stdout, qr/Usage: $CMD/, 'Testing stdout';
like stderr, qr/Ontospy v[0-9.]+/, 'Testing stderr';

done_testing;
