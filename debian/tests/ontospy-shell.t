use strict;
use warnings;

use Test::More;
use Test::Command::Simple;
use File::HomeDir::Test;
use File::HomeDir;
use File::Path qw(make_path);
use Test::TempDir::Tiny;
use Test::Expect;

my @CMD = qw/ontospy shell/;
my $CMD = join ' ', @CMD;

run_ok @CMD, '--help';
like stdout, qr/Usage: $CMD/, 'Testing stdout';
like stderr, qr/Ontospy v[0-9.]+/, 'Testing stderr';

ok $ENV{HOME} = File::HomeDir->my_home, 'create fake $HOME';
ok make_path "$ENV{HOME}/.ontospy/models", 'create $HOME/.ontospy/models';

in_tempdir "shell" => sub {
	expect_run(
		command => [@CMD],
		prompt  => [ -re => 'Ontospy\]\S+>\s+' ],
		quit    => 'quit',
	);
	expect_send('help', 'request help');
	expect_like(qr/Commands available/, 'expect help');
	expect_send('help foo', 'request help on foo');
	expect_like(qr/No help on foo/, 'expect no help on foo');
	expect_send('help help', 'request help on help');
	expect_like(qr/List available commands with "help" or detailed help with "help cmd"/, 'expect help on help');
	expect_send('info', 'request info');
	expect_like(qr/No graph selected/, 'expect no info');
	expect_quit;
};

done_testing;
