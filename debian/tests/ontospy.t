use strict;
use warnings;

use Test::More;
use Test::Command::Simple;

my $CMD = 'ontospy';

run_ok $CMD;
cmp_ok stdout, 'eq', '', 'Testing stdout';
like stderr, qr/Usage: $CMD/, 'Testing stderr';

run_ok $CMD, '--help';
like stdout, qr/Usage: $CMD/, 'Testing stdout';
cmp_ok stderr, 'eq', '', 'Testing stderr';

done_testing;
