use strict;
use warnings;

use Test::More;
use Test::Command::Simple;
use Cwd qw(cwd);
use File::HomeDir::Test;
use File::HomeDir;
use File::Path qw(make_path);
use Test::TempDir::Tiny;

my @CMD = qw/ontospy gendocs/;
my $CMD = join ' ', @CMD;

run_ok @CMD;
cmp_ok stdout, 'eq', '', 'Testing stdout';
like stderr, qr/Usage: $CMD/, 'Testing stderr';

run_ok @CMD, '--help';
like stdout, qr/Usage: $CMD/, 'Testing stdout';
like stderr, qr/Ontospy v[0-9.]+/, 'Testing stderr';

my $FOAF = 'file://' . cwd . '/ontospy/tests/rdf/foaf.rdf';

ok $ENV{HOME} = File::HomeDir->my_home, 'create fake $HOME';
ok make_path "$ENV{HOME}/.ontospy/models", 'create $HOME/.ontospy/models';

in_tempdir "gendocs" => sub {
	run_ok @CMD, qw(--type 1), $FOAF;
	cmp_ok stdout, 'eq', '', 'Testing stdout';
	like stderr, qr/Loading graph.*\nBuilding visualization/, 'Testing stderr';

	run_ok @CMD, qw(--type 3), $FOAF;
	cmp_ok stdout, 'eq', '', 'Testing stdout';
	like stderr, qr/Loading graph.*\nBuilding visualization/, 'Testing stderr';
};

done_testing;
